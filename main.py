from fastapi import FastAPI
from routers.biometrics_routes import faceRecognition

app = FastAPI()

app.include_router(faceRecognition)
import base64
import os
import logging


def base64_to_image(img_string_b64, name, path="resources/decoded_images/", extension='png'):
    """
    Metodo para hacer la decodificacion de la imagen en base64
    :param img_string_b64: string base 64 de la imagen
    :param name: nombre con el que se guarda la imagen decodificada
    :param path: ruta donde se guardara la imagen decodificada
    :param extension: extension con la que se guarda la imagen
    :return: guarda la imagen decodificada en 'resources/decoded_images/'
    """
    try:
        imgdata = base64.b64decode(img_string_b64)
        filename = os.path.join(path, name + "." + extension)
        with open(filename, 'wb') as f:
            f.write(imgdata)
        return filename
    except Exception as ex:
        logging.error("Error al decodificar la imagen en base64 {}".format(ex))
        logging.error("path_image {}".format(name))
        return "Error al decodificar "


def image_to_base64(img_path):
    try:
        with open(img_path, 'rb') as img_file:
            base64_bytes = base64.b64decode(img_file.read())
            return base64_bytes
    except Exception as ex:
        logging.error("Error al codificar la imagen a base64".format(ex))
        return "Error al codificar"
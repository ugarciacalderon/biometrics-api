from fastapi import APIRouter
from fastapi.responses import JSONResponse
import time

import database
from utils.utils import *
from biometrics.biometrics import *
from database import *

faceRecognition = APIRouter()

"""
curl -X 'GET' \
  'http://127.0.0.1:8000/faceCompare/?image_qr_path=%2FVolumes%2FDisk%2FDocuments%2F03_DERFE_SIADC%2FWORKSPACE_ML%2Fbiometrics-scripts%2Fmuestras%2Fjorgeocr.png&image_selfie_path=%2FVolumes%2FDisk%2FDocuments%2F03_DERFE_SIADC%2FWORKSPACE_ML%2Fbiometrics-scripts%2Fselfies%2Fjorge1.png' \
  -H 'accept: application/json'
"""


@faceRecognition.get("/")
async def root():
    content = {"message": "Se esta ejecutando el api para Validación Biométrica"}
    headers = {"X-Web-Framework": "FastAPI", "Content-Language": "en-US"}
    return JSONResponse(content=content, headers=headers)


@faceRecognition.get("/hello/{name}")
async def say_hello(name: str):
    content = {"message": f"{name}"}
    headers = {"X-Web-Framework": "FastAPI", "Content-Language": "en-US"}
    return JSONResponse(content=content, headers=headers)


@faceRecognition.get("/validateFaces/")
async def validateFaces(image_qr_b64: str, image_selfie_b64: str, tolerance=0.5):
    b64OrPath = True
    start_time = time.time()
    image_qr = base64_to_image(image_qr_b64, "imageQR")
    image_selfie = base64_to_image(image_selfie_b64, "imageSelfie")
    response = biometrics_validation(image_qr, image_selfie, b64OrPath, tolerance)
    end_time = time.time()
    content = {"equals": str(response[0]),
               "distance": str(response[1]),
               "time": str(end_time - start_time)}

    headers = {"X-Web-Framework": "FastAPI", "Content-Language": "en-US"}
    return JSONResponse(content=content, headers=headers)


@faceRecognition.get("/faceCompare/")
async def faceCompare(image_qr_path: str, image_selfie_path: str, tolerance=0.5):
    b64OrPath = False
    start_time = time.time()
    response = biometrics_validation(image_qr_path, image_selfie_path, b64OrPath, tolerance)
    end_time = time.time()
    content = {"equals": str(response[0]),
               "distance": str(response[1]),
               "time": str(end_time - start_time)}

    headers = {"X-Web-Framework": "FastAPI", "Content-Language": "en-US"}
    return JSONResponse(content=content, headers=headers)


@faceRecognition.get("/testDBConnection/")
async def testDBConnection():
    messagge = database.connection_oracle()
    content = {"message": f"{messagge}"}
    headers = {"X-Web-Framework": "FastAPI", "Content-Language": "en-US"}
    return JSONResponse(content=content, headers=headers)

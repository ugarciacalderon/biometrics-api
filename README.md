## Parameter tolerance

| decimal  | evivalencia en % |
| ------------- | ------------- |
| 0.1  | 10%  |
| 0.2  | 20%  |
| 0.3  | 30%  |
| 0.4  | 40%  |
| 0.5  | 50%  |
| 0.6  | 60%  |
| 0.7  | 70%  |
| 0.8  | 80%  |
| 0.9  | 90%  |
| 1.0  | 100%  |

Cuando el valor es menor o igual a 0.5, es mas estricto con la validación, para determinar si son autneticos los rostros comparados
si no especifica algun valor decimal, por defecto tiene una toleranica de 0.6.
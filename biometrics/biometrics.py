import face_recognition
import logging
import os

def biometrics_validation(image_qr: str, image_selfie: str, b64OrPath: bool, approbation: float):
    """
    Metodo para hacer la comparacion facial y obtener la distancia que existe entre las imagenes comparadas
    :param image_qr: imagen de referencia utilizada para validar la selfie
    :param image_selfie: imagen selfie que se va a validar
    :return: Boolean y Double
    """
    try:
        # load image
        credential_image = face_recognition.load_image_file(image_qr)
        selfie_image = face_recognition.load_image_file(image_selfie)
        # find faces
        face_credential_detected = face_recognition.face_locations(credential_image)
        face_selfie_detected = face_recognition.face_locations(selfie_image)
        if len(face_credential_detected) > 0 and len(face_selfie_detected) > 0:
            # encode image
            credential_encoding = face_recognition.face_encodings(credential_image)[0]
            selfie_encoding = face_recognition.face_encodings(selfie_image)[0]
            # face match validation
            face_distances = face_recognition.face_distance([credential_encoding], selfie_encoding)
            results = face_recognition.compare_faces([credential_encoding], selfie_encoding, tolerance=float(approbation))
            #delete images from decode_images
            if b64OrPath:
                os.remove(image_qr)
                os.remove(image_selfie)
        else:
            return str('False'), str(0)

        return str(results[0]), str(face_distances[0])

    except Exception as ex:
        logging.error("Error al validar los rostros {}".format(ex))
        return str("Error al validar los rostros"), str(0)

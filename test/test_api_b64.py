import base64
import os
import sys
import time

import requests

'''
    run script: python3 test_api_b64.py /Volumes/Disk/Downloads/lfw 6 /Users/ulisesgc/Desktop/test/resultsb64.txt
'''
def generaJson(image_qr_path, image_selfie_path, tolerance):
    payload = {'image_qr_b64': image_qr_b64,
               'image_selfie_b64': image_selfie_b64,
               'tolerance': tolerance
               }
    return payload

def escribeArchivo(image_qr_path, image_qr_b64, image_selfie_path, image_selfie_b64, tolerance, response):
    with open(txt_output_path, 'a+') as file:
        file.write('--------------------------------------------------------------------------------- \n')
        file.write('image_qr_b64: ' + image_qr_path + '\n')
        file.write('********************************************************************************* \n')
        file.write(str(image_qr_b64) + '\n')
        file.write('********************************************************************************* \n')
        file.write('image_selfie_path: ' + image_selfie_path + '\n')
        file.write('********************************************************************************* \n')
        file.write(str(image_selfie_b64) + '\n')
        file.write('********************************************************************************* \n')
        file.write('tolerance: ' + tolerance + '\n')
        file.write('*********************************************************** \n')
        file.write(str(response.json()) + '\n')
        file.write('*********************************************************** \n')
        file.write('--------------------------------------------------------------------------------- \n')
        file.write('\n')


def escribeResumenArchivo(counter_global, counter_analyzed, counter_succe, counter_fails):
    with open(txt_output_path, 'a+') as file:
        file.write('==========================================RESUMEN==========================================\n')
        file.write("Total directorios: {} \n".format(int(counter_global)))
        file.write("Analizados: {} \n".format(int(counter_analyzed)))
        file.write("Exitosos: {} \n".format(int(counter_succe)))
        file.write("Erroneos: {} \n".format(int(counter_fails)))
        file.write('===========================================================================================\n')


def analizar__next_item():
    pass

path = sys.argv[1]
tolerance = sys.argv[2]
txt_output_path = sys.argv[3]

counter_fails = 0
counter_succe = 0
counter_global = 0
counter_analyzed = 0

fname = []
url = "http://localhost:8000/validateFaces/"


start_time = time.time()
for root, d_names, f_names in os.walk(path):
    counter_global = counter_global + 1
    if len(f_names) >= 2:
        counter_analyzed = counter_analyzed + 1
        image_qr_path = root + '/' + f_names[0]
        image_selfie_path = root + '/' + f_names[1]

        with open(image_qr_path, 'rb') as image_qr:
            image_qr_b64 = base64.b64encode(image_qr.read())

        with open(image_selfie_path, 'rb') as image_selfie:
            image_selfie_b64 = base64.b64encode(image_selfie.read())

        # genera json
        payload = generaJson(image_qr_path, image_selfie_path, tolerance)

        response = requests.get(url, params=payload)
        equals = response.json()
        if 'equals' not in equals:
            equals['equals'] = 'False'

        equals = equals['equals']

        counter_fails = counter_fails + 1 if equals.__eq__('False') else counter_fails + 0
        counter_succe = counter_succe + 1 if equals.__eq__('True') else counter_succe + 0

        # escribe en el archivo las imagenes utilizadas asi como la cadena base64
        escribeArchivo(image_qr_path, image_qr_b64, image_selfie_path, image_selfie_b64, tolerance, response)

# escribe el resumen de las pruebas
escribeResumenArchivo(counter_global, counter_analyzed, counter_succe, counter_fails)

end_time = time.time()
total_time = end_time - start_time
mins, secs = divmod(total_time, 60)
hours, mins = divmod(mins, 60)
print('total time: %d:%d:%d {}'.format(str(hours) + ':' + str(mins) + ':' + str(secs)))

import os
import sys
import time

import requests

'''
    run script: python3 test_api_paths.py /Volumes/Disk/Downloads/lfw 6 /Users/ulisesgc/Desktop/test/resultsPaths.txt
'''

def generaJson(image_qr_path, image_selfie_path, tolerance):
    payload = {'image_qr_path': image_qr_path,
               'image_selfie_path': image_selfie_path,
               'tolerance': tolerance
               }
    return payload

def escribeArchivo(image_qr_path, image_selfie_path, tolerance, response):
    with open(txt_output_path, 'a+') as file:
        file.write('--------------------------------------------------------------------------------- \n')
        file.write('image_qr_path: ' + image_qr_path + '\n')
        file.write('image_selfie_path: ' + image_selfie_path + '\n')
        file.write('tolerance: ' + tolerance + '\n')
        file.write('*********************************************************** \n')
        file.write(str(response.json()) + '\n')
        file.write('*********************************************************** \n')
        file.write('--------------------------------------------------------------------------------- \n')
        file.write('\n')

def escribeResumenArchivo(counter_global, counter_analyzed, counter_succe, counter_fails):
    with open(txt_output_path, 'a+') as file:
        file.write('==========================================RESUMEN==========================================\n')
        file.write("Total directorios: {} \n".format(int(counter_global)))
        file.write("Analizados: {} \n".format(int(counter_analyzed)))
        file.write("Exitosos: {} \n".format(int(counter_succe)))
        file.write("Erroneos: {} \n".format(int(counter_fails)))
        file.write('===========================================================================================\n')

'''
    Esta funcion sirve para analizar las imagenes que solo estan en la raiz de un directorio,
    que no contiene subdirectorios o clasificaciones previas
'''
def analizar__next_item():
    pass


path = sys.argv[1]
tolerance = sys.argv[2]
txt_output_path = sys.argv[3]

counter_fails = 0
counter_succe = 0
counter_global = 0
counter_analyzed = 0

fname = []
url = "http://localhost:8000/faceCompare/"
start_time = time.time()
for root, d_names, f_names in os.walk(path):
    counter_global = counter_global + 1
    if len(f_names) >= 2:
        counter_analyzed = counter_analyzed + 1
        image_qr_path = root + '/' + f_names[0]
        image_selfie_path = root + '/' + f_names[1]

        # genera json
        payload = generaJson(image_qr_path, image_selfie_path, tolerance)
        response = requests.get(url, params=payload)
        equals = response.json()
        equals = equals['equals']

        counter_fails = counter_fails + 1 if equals.__eq__('False') else counter_fails + 0
        counter_succe = counter_succe + 1 if equals.__eq__('True') else counter_succe + 0
        # escribe la informacion de las imagenes analizadas
        escribeArchivo(image_qr_path, image_selfie_path, tolerance, response)

# escribe el resumen de las pruebas
escribeResumenArchivo(counter_global, counter_analyzed, counter_succe, counter_fails)

end_time = time.time()
total_time = end_time - start_time
mins, secs = divmod(total_time, 60)
hours, mins = divmod(mins, 60)
print('total time: %d:%d:%d {}'.format(str(hours) + ':' + str(mins) + ':' + str(secs)))




